import java.util.*;

class Box
{
	int height;
	int width;
	int depth;

	public Box()
	{
		height = width = depth = 0;
	}

	public void setDimensions(int h, int w, int d)
	{
		height = h;
		width = w;
		depth = d;
	}

	public int getVolume()
	{
		return height * width * depth;
	}

	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		int h, w, d;
		Box box = new Box();
		System.out.print("\nDimensions (height, width, depth): ");
		h = in.nextInt();
		w = in.nextInt();
		d = in.nextInt();
		box.setDimensions(h, w, d);
		System.out.println("Volume: " + box.getVolume());
	}
}