<!DOCTYPE html>
<html>
<head>
	<title>Student Marks</title>
</head>
<body>
	<?php $xml=simplexml_load_file("students.xml") or die("Error:Cannot open object"); ?>
	<p>
		<?php
			foreach($xml->student as $student) {
				$rankwise[] = $student;
			}
			function cmp_marks($s1, $s2) {
				return ((int)$s1->mark < (int)$s2->mark) ? 1 : -1;
			}
			usort($rankwise, "cmp_marks");
		?>
		Ranklist:
		<ul>
			<?php
				foreach ($rankwise as $student) {
					echo '<li>'.$student->name.': '.$student->mark.'</li>';
				}
			?>
		</ul>
	</p>
	<p>
		Class topper:
		<?php echo $rankwise[0]->name ?>
	</p>
	<p>
		Placement results:
		<ul>
			<?php
				foreach ($xml->student as $student) {
					if (strcmp($student->company, "None")) {
						echo '<li>'.$student->name.' placed at '.$student->company.'</li>';
					}
				}
			?>
		</ul>
	</p>
</body>
</html>