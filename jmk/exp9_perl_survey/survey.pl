#!C:\Perl\bin\perl.exe

use CGI;
$query = new CGI;

print $query->header;
print $query->start_html('Student Survey');

if (!$query->param)
{
	print $query->startform;

	print $query->p;
	print "Name: ";
	print $query->textfield(-name=>'name', -size=>35, maxlength=>50);

	print $query->p;
	print "Age: ";
	print $query->textfield(-name=>'age', -size=>2, -maxlength=>3);

	print $query->p;
	print "Sex: ";
	print $query->radio_group(-name=>'sex', -values=>['Male','Female']);

	print $query->p;
	print "University: ";
	print $query->radio_group(-name=>'university', -values=>['MGU','KTU','Independent']);

	print $query->p;
	print "College: ";
	print $query->textfield(-name=>'college', -size=>35, -maxlength=>50);

	print $query->p;
	print "Course: ";
	print $query->textfield(-name=>'course', -size=>35, -maxlength=>50);

	print $query->p;
	print "Which programming language would you most like to learn? ";
	print $query->br;
	print $query->radio_group(-name=>'lang', -values=>['C','Java','Python', 'Other']);

	print $query->p;
	print $query->submit(-value=>'Submit');
	print $query->endform;
} 
else
{
	print $query->h3('Results:');
	
	print "Name: ";
	print $query->param('name');
	print $query->br;
	
	print "Age: ";
	print $query->param('age');
	print $query->br;
	
	print "Sex: ";
	print $query->param('sex');
	print $query->br;
	
	print "University: ";
	print $query->param('university');
	print $query->br;
	
	print "College: ";
	print $query->param('college');
	print $query->br;
	
	print "Course: ";
	print $query->param('course');
	print $query->br;
	
	print "Language choice: ";
	print $query->param('lang');
	print $query->br;
}

print $query->end_html;