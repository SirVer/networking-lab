import java.util.*;

class Fibonacci
{
	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		int a = 1, b = 1, c, n, i;
		System.out.print("n: ");
		n = in.nextInt();
		if (n >= 1)
			System.out.print(a + " ");
		if (n >= 2)
			System.out.print(b + " ");
		if (n > 2)
			for (i = 3; i <= n; i++,  a = b, b = c)
			{
				c = a + b;
				System.out.print(c + " ");
			}
		System.out.println("\n");
	}
}