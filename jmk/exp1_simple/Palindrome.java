import java.util.*;

class Palindrome
{
	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		String s;
		System.out.print("Enter sentence: ");
		s = in.nextLine();
		s = s.toLowerCase().replaceAll("[^a-z0-9]", "");
		if (new StringBuilder(s).reverse().toString().equals(s))
			System.out.println("Palindrome.");
		else
			System.out.println("Not a palindrome.");
	}
}