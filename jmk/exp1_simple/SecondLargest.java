import java.util.*;

class SecondLargest
{
	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		int[] arr;
		int n, i, max, sec_max;
		System.out.print("No. of numbers: ");
		n = in.nextInt();
		arr = new int[n];
		System.out.print("Numbers: ");
		for (i = 0; i < n; i++)
			arr[i] = in.nextInt();
		for (i = 0, max = 0; i < n; i++)
			if (arr[i] > arr[max])
				max = i;
		for (i = 0, sec_max = (max+1)%n; i < n; i++)
			if (arr[i] > arr[sec_max] && arr[i] < arr[max])
				sec_max = i;
		System.out.println("Second largest: " + arr[sec_max]);
	}
}