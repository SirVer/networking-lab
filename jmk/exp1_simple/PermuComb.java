import java.util.*;

class PermuComb
{
	static int factorial(int x)
	{
		if (x == 0)
			return 1;
		return x * factorial(x-1);
	}

	public static void main(String args[])
	{
		Scanner in = new Scanner(System.in);
		int n, r, C, P;
		System.out.print("n: ");
		n = in.nextInt();
		System.out.print("r: ");
		r = in.nextInt();
		C = factorial(n)/(factorial(n-r)*factorial(r));
		P = factorial(n)/factorial(n-r);
		System.out.println("nCr: " + C + "\nnPr: " + P);
	}
}