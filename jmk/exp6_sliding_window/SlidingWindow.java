/*
Add a method to sender to get the text in the TextField
Add a string to sender to store the text in the TextField
Use nt and/or na (as index) to figure out which characters in the string to send
Look into a self socket connection
If it is possible, then we can use the BufferedReader to take in character at a time
on receiver end, using sleep() calls or something to delay things
If not, try to set up some sort of buffer between sender and receiver
*/
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

class SlidingSender {
	JPanel senderPanel;
	JButton start;
	JTextField send;
	int windowSize = 3;
	int nt = 0, na = 0;
	SlidingSender() {
		senderPanel = new JPanel();
		send = new JTextField(10);
		start = new JButton("Start");
		senderPanel.add(send);
		senderPanel.add(start);
		senderPanel.setBounds(0, 0, 400, 200);
	}
}

class SlidingReceiver {
	JPanel receiverPanel;
	JTextField receive;
	int windowSize = 3;
	int nr = 0, ns = 0;
	SlidingReceiver() {
		receiverPanel = new JPanel();
		receive = new JTextField(10);
		receiverPanel.add(receive);
		receiverPanel.setBounds(0, 400, 400, 100);
	}
}

class SlidingMonitor {
	JPanel monitorPanel;
	JTextField[] window;
	SlidingMonitor() {
		monitorPanel = new JPanel();
		window = new JTextField[3];
		window[0] = new JTextField(2);
		window[1] = new JTextField(2);
		window[2] = new JTextField(2);
		monitorPanel.add(window[0]);
		monitorPanel.add(window[1]);
		monitorPanel.add(window[2]);
		monitorPanel.setBounds(0, 200, 400, 200);
	}
}

class SlidingWindow {
	static JPanel senderPanel, receiverPanel, monitorPanel;
	static JFrame f;
	public static void main (String[] args) {
		senderPanel = new SlidingSender().senderPanel;
		receiverPanel = new SlidingReceiver().receiverPanel;
		monitorPanel = new SlidingMonitor().monitorPanel;
		f = new JFrame();
		f.add(senderPanel);
		f.add(receiverPanel);
		f.add(monitorPanel);
		f.setSize(400, 500);
		f.setLayout(null);
		f.setVisible(true);
	}
}