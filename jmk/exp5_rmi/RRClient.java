import java.rmi.*;
import java.util.Scanner;

public class RRClient{
	public static void main(String args[]) {
		Scanner kb_input = new Scanner(System.in);
		String input;
		while (true) {
			System.out.print("Enter string: ");
			input = kb_input.nextLine();
			try {
				Reverse stub = (Reverse)Naming.lookup("rmi://localhost:5000/remoterev");
				System.out.println("Reverse: " + stub.reverse(input) + "\n");
			} catch(Exception e) {
				System.out.println(e);
			}
		}
	}
}