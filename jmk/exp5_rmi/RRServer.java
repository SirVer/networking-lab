import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

interface Reverse extends Remote {
	public String reverse(String str) throws RemoteException;
}

class ReverseRemote extends UnicastRemoteObject implements Reverse {
	ReverseRemote() throws RemoteException {
		super();
	}
	public String reverse(String str) {
		String rev_str = new StringBuilder(str).reverse().toString();
		return rev_str;
	}
}

public class RRServer {
	public static void main(String args[]) {
		try {
			Reverse stub = new ReverseRemote();
			Naming.rebind("rmi://localhost:5000/remoterev",stub);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}