import java.io.IOException;
import java.net.*;

public class BroadcastClient
{
	private static String hostname= "localhost";
	private static int port=1234;
	private static InetAddress host;
	private static DatagramSocket socket;
	static DatagramPacket packet;

	public static void main(String[] args)
	{
		try
		{
			while (true)
			{
				host = InetAddress.getByName(hostname);
				socket = new DatagramSocket (null);
				packet = new DatagramPacket (new byte[100], 0,host, port);
				socket.send (packet);
				packet.setLength(100);
				socket.receive (packet);
				socket.close ();
				byte[] data = packet.getData ();
				String time=new String(data);  // convert byte array data into string
				System.out.println(time);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
