import java.io.IOException;
import java.net.*;

public class BroadcastServer
{
	public static final int DEFAULT_PORT = 1234;
	private static DatagramSocket socket;
	private static DatagramPacket packet;

	public static void main(String[] args) throws InterruptedException
	{
		try
		{
			socket = new DatagramSocket(DEFAULT_PORT);
		}
		catch( Exception ex )
		{
			System.out.println("Problem creating socket on port: " + DEFAULT_PORT );
		}
		packet = new DatagramPacket (new byte[1], 1);
		System.out.println("Joseph Monis, Roll No. 58");
		while (true)
		{
			try
			{
				socket.receive (packet);
				System.out.println("Sent to: " + packet.getAddress () + ":" +
								   packet.getPort ());
				byte[] outBuffer = new java.util.Date ().toString ().getBytes ();
				packet.setData (outBuffer);
				packet.setLength (outBuffer.length);
				socket.send (packet);
				Thread.sleep(500);
			}
			catch (IOException ie)
			{
				ie.printStackTrace();
			}
		}
	}
}