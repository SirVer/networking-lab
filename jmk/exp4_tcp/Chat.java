import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

public class Chat implements ActionListener {
	static JTextField msgInput;
	static JTextArea chatHistory;
	static JButton submit, clear, openConn;
	static OutputStream socketOutputStream;
	static PrintWriter socketWriter;
	static InputStream socketInputStream;
	static BufferedReader socketReader;
	static String rcvMessage, sendMessage;
	static int isClientOrServerFlag;
	static Socket sock;
	static ServerSocket servSock;
	public static JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();
		msgInput = new JTextField(20);
		submit = new JButton("Submit");
		submit.addActionListener(new Chat());
		clear = new JButton("Clear");
		clear.addActionListener(new Chat());
		inputPanel.add(msgInput);
		inputPanel.add(submit);
		inputPanel.add(clear);
		inputPanel.setBounds(0, 400, 400, 65);
		return inputPanel;
	}
	public static JPanel createOutputPanel() {
		JPanel outputPanel = new JPanel();
		chatHistory = new JTextArea(100, 35);
		outputPanel.setBounds(0, 0, 400, 400);
		outputPanel.add(chatHistory);
		return outputPanel;
	}
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals("Submit")) {
			sendMessage = msgInput.getText();
			msgInput.setText("");
			socketWriter.println(sendMessage);
			socketWriter.flush();
			chatHistory.append("You: " + sendMessage + "\n");
		}
		else if (str.equals("Clear"))
			chatHistory.setText("");
	}
	public static void main(String[] args) {
		isClientOrServerFlag = 0;
		if (args.length > 1) {
			System.out.println("Usage: java Chat [-S]");
			System.out.println("\t-S\tStart server instance");
			return;
		}
		if (args.length != 0 && args[0].equals("-S")) {
			isClientOrServerFlag = 1;
			try {
				servSock = new ServerSocket(3000);
			}
			catch (IOException ex) {
				System.out.println("ERROR: Couldn't create server socket on port 3000");
			}
		}
		else {
			isClientOrServerFlag = 0;
			try {
				sock = new Socket("localhost", 3000);
			}
			catch (IOException ex) {
				System.out.println("ERROR: Couldn't connect to server at port 3000");
			}
		}
		JPanel inputPanel = createInputPanel();
		JPanel outputPanel = createOutputPanel();
		JFrame f = new JFrame();
		f.setSize(405, 500);
		f.setLayout(null);
		f.add(outputPanel);
		f.add(inputPanel);
		f.setVisible(true);
		f.setResizable(false);
		f.getRootPane().setDefaultButton(submit);
		// For sending through socket
		if (isClientOrServerFlag == 1)
		{
			try {
				sock = servSock.accept();
			}
			catch (IOException ex) {
				System.out.println("ERROR: Couldn't get socket output stream");
			}
		}
		try {
			socketOutputStream = sock.getOutputStream();
		}
		catch (IOException ex) {
			System.out.println("ERROR: Couldn't get socket output stream");
		}
		socketWriter = new PrintWriter(socketOutputStream, true);
		// For receiving from socket
		try {
			socketInputStream = sock.getInputStream();
		}
		catch (IOException ex) {
			System.out.println("ERROR: Couldn't get socket input stream");
		}
		socketReader = new BufferedReader(new InputStreamReader(socketInputStream));
		while (true) {
			try {
				if ((rcvMessage = socketReader.readLine()) != null)
					chatHistory.append("Other: " + rcvMessage + "\n");
			}
			catch (IOException ex) {
				System.out.println("ERROR: Couldn't read from socket input");
			}
		}
	}
}