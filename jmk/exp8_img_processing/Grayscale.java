import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Grayscale {
	static BufferedImage image;
	static BufferedImage gray_image;
	static int width;
	static int height;
	public static void convert(String filename) {
		try {
			File input = new File(filename);
			image = ImageIO.read(input);
			gray_image = ImageIO.read(input);
			width = gray_image.getWidth();
			height = gray_image.getHeight();
			for (int i = 0; i < height; i++){
				for (int j = 0; j < width; j++){
					Color c = new Color(gray_image.getRGB(j, i));
					int red = (int)(c.getRed() * 0.299);
					int green = (int)(c.getGreen() * 0.587);
					int blue = (int)(c.getBlue() * 0.114);
					Color newColor = new Color(red+green+blue, red+green+blue, red+green+blue);
					gray_image.setRGB(j, i, newColor.getRGB());
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void main(String args[]) throws Exception 
	{
		convert(args[0]);
		JFrame f = new JFrame();
		JLabel imageLabel = new JLabel(new ImageIcon(image));
		JLabel gray_imageLabel = new JLabel(new ImageIcon(gray_image));
		f.setSize(width, (height*2)+40);
		f.setLayout(new FlowLayout());
        f.setResizable(false);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setTitle("Grayscale");
		imageLabel.setVisible(true);
		gray_imageLabel.setVisible(true);
		f.add(imageLabel);
		f.add(gray_imageLabel);
	}
}