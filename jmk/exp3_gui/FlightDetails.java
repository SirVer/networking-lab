import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
 
public class FlightDetails implements ActionListener {
	static JTextArea outputA, outputB;
	static JTextField name, pnr, seatNum, flightNum, src, dest, eta;
	static JTextField nameA, pnrA, seatNumA, flightNumA, srcA, destA, etaA;
	public static JPanel createInputPanel(String panelName, String submitButtonName, String clearButtonName) {
		JPanel inputPanel = new JPanel();
		JLabel label_panelName = new JLabel(panelName);
		JLabel label_name = new JLabel("Name:");
		JLabel label_pnr = new JLabel("PNR:");
		JLabel label_seatNum = new JLabel("Seat No.:");
		JLabel label_flightNum = new JLabel("Flight No.:");
		JLabel label_src = new JLabel("Source:");
		JLabel label_dest = new JLabel("Destination:");
		JLabel label_eta = new JLabel("ETA:");
		JButton submit, clear;
		submit = new JButton(submitButtonName);
		submit.addActionListener(new FlightDetails());
		clear = new JButton(clearButtonName);
		clear.addActionListener(new FlightDetails());
		nameA = name;
		name = new JTextField(15);
		pnrA = pnr;
		pnr = new JTextField(5);
		seatNumA = seatNum;
		seatNum = new JTextField(5);
		flightNumA = flightNum;
		flightNum = new JTextField(5);
		srcA = src;
		src = new JTextField(15);
		destA = dest;
		dest = new JTextField(15);
		etaA = eta;
		eta = new JTextField(5);
		inputPanel.add(label_panelName);
		inputPanel.add(label_name);
		inputPanel.add(name);
		inputPanel.add(label_seatNum);
		inputPanel.add(pnr);
		inputPanel.add(label_pnr);
		inputPanel.add(seatNum);
		inputPanel.add(label_flightNum);
		inputPanel.add(flightNum);
		inputPanel.add(label_src);
		inputPanel.add(src);
		inputPanel.add(label_dest);
		inputPanel.add(dest);
		inputPanel.add(label_eta);
		inputPanel.add(eta);
		inputPanel.add(submit);
		inputPanel.add(clear);
		return inputPanel;
	}
	public static JPanel createOutputPanel() {
		JPanel outputPanel = new JPanel();
		outputA = new JTextArea();
		outputB = new JTextArea();
		outputPanel.setBounds(0, 250, 400, 150);
		outputPanel.add(outputA);
		outputPanel.add(outputB);
		return outputPanel;
	}
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals("Submit A") || str.equals("Submit B"))
		{
			String disp_str;
			if (str.equals("Submit A"))
			{
				disp_str = "Flight A\nName: " + nameA.getText() + "\n"
						+ "PNR: " + pnrA.getText() + "\n"
						+ "Seat No.: " + seatNumA.getText() + "\n"
						+ "Flight No.: " + flightNumA.getText() + "\n"
						+ "Source: " + srcA.getText() + "\n"
						+ "Destination: " + destA.getText() + "\n"
						+ "ETA: " + etaA.getText();
				outputA.setText("");
				outputA.setText(disp_str);
			}
			else if (str.equals("Submit B"))
			{
				disp_str = "Flight B\nName: " + name.getText() + "\n"
						+ "PNR: " + pnr.getText() + "\n"
						+ "Seat No.: " + seatNum.getText() + "\n"
						+ "Flight No.: " + flightNum.getText() + "\n"
						+ "Source: " + src.getText() + "\n"
						+ "Destination: " + dest.getText() + "\n"
						+ "ETA: " + eta.getText();
				outputB.setText("");
				outputB.setText(disp_str);
			}
		}
		else if (str.equals("Clear A"))
		{
			nameA.setText("");
			pnrA.setText("");
			seatNumA.setText("");
			flightNumA.setText("");
			srcA.setText("");
			destA.setText("");
			etaA.setText("");
		}
		else if (str.equals("Clear B"))
		{
			name.setText("");
			pnr.setText("");
			seatNum.setText("");
			flightNum.setText("");
			src.setText("");
			dest.setText("");
			eta.setText("");
		}
	}
	public static void main(String[] args) {
		JPanel inputPanelA = createInputPanel("Flight A", "Submit A", "Clear A");
		JPanel inputPanelB = createInputPanel("Flight B", "Submit B", "Clear B");
		JPanel outputPanel = createOutputPanel();
		JFrame f = new JFrame();
		inputPanelA.setBounds(0, 0, 200, 300);
		inputPanelB.setBounds(200, 0, 200, 300);
		f.add(inputPanelA);
		f.add(inputPanelB);
		f.add(outputPanel);
		f.setSize(400, 500);
		f.setLayout(null);
		f.setVisible(true);
		f.setResizable(false);
	}
}