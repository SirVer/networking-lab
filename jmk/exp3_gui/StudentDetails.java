import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StudentDetails implements ActionListener {
	static JTextArea output;
	static JTextField name, branch, rollNo, year, aggMarks;
	static JButton submit, clear;
	public static JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();
		JLabel label_name = new JLabel("Name:");
		JLabel label_rollNo = new JLabel("Roll No.:");
		JLabel label_branch = new JLabel("Branch:");
		JLabel label_year = new JLabel("Year:");
		JLabel label_aggMarks = new JLabel("Aggregate Marks:");
		submit = new JButton("Submit");
		submit.addActionListener(new StudentDetails());
		clear = new JButton("Clear");
		clear.addActionListener(new StudentDetails());
		name = new JTextField(35);
		branch = new JTextField(35);
		rollNo = new JTextField(7);
		year = new JTextField(7);
		aggMarks = new JTextField(7);
		inputPanel.add(label_name);
		inputPanel.add(name);
		inputPanel.add(label_branch);
		inputPanel.add(branch);
		inputPanel.add(label_rollNo);
		inputPanel.add(rollNo);
		inputPanel.add(label_year);
		inputPanel.add(year);
		inputPanel.add(label_aggMarks);
		inputPanel.add(aggMarks);
		inputPanel.add(submit);
		inputPanel.add(clear);
		inputPanel.setBounds(0, 0, 400, 300);
		return inputPanel;
	}

	public static JPanel createOutputPanel() {
		JPanel outputPanel = new JPanel();
		output = new JTextArea();
		outputPanel.setBounds(0, 250, 400, 150);
		outputPanel.add(output);
		return outputPanel;
	}

	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals("Submit"))
		{
			String disp_str;
			output.setText("");
			disp_str = "Name: " + name.getText() + "\n"
					+ "Roll no.: " + rollNo.getText() + "\n"
					+ "Branch: " + branch.getText() + "\n"
					+ "Year: " + year.getText() + "\n"
					+ "Aggregate marks: " + aggMarks.getText();
			output.setText(disp_str);
		}
		else if (str.equals("Clear"))
		{
			name.setText("");
			rollNo.setText("");
			branch.setText("");
			year.setText("");
			aggMarks.setText("");
		}
	}

	public static void main(String[] args) {
		JPanel inputPanel = createInputPanel();
		JPanel outputPanel = createOutputPanel();
		JFrame f = new JFrame();
		f.add(inputPanel);f.add(outputPanel);
		f.setSize(400, 400);
		f.setLayout(null);
		f.setVisible(true);
		f.setResizable(false);
	}
}