import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
class Window extends JFrame implements ActionListener
{
	JTextField t1, t2, t3, t4, t5, t6;
	JTextArea creds;
	JLabel l1, l2;
	JButton b1, b2;
	public Window()
	{
		setSize(310, 240);
		setVisible(true);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t1 = new JTextField(10);
		t2 = new JTextField(10);
		t3 = new JTextField(3);
		t4 = new JTextField(3);
		t5 = new JTextField(3);
		l1 = new JLabel("Sender");
		l2 = new JLabel("Receiver");
		creds = new JTextArea("Done by:\nJoseph Monis\nRoll No. 58");
		t1.setBounds(10, 30, 100, 20);
		t2.setBounds(200, 30, 100, 20);
		t3.setBounds(75, 120, 50, 20);
		t4.setBounds(125, 120, 50, 20);
		t5.setBounds(175, 120, 50, 20);
		l1.setBounds(10, 10, 100, 20);
		l2.setBounds(200, 10, 100, 20);
		creds.setBounds(75, 170, 150, 50);
		add(l1);
		add(l2);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		add(t5);
		add(creds);
		b1 = new JButton("Send");
		b2 = new JButton("Receive");
		b1.setBounds(10, 70, 100, 20);
		b2.setBounds(200, 70, 100, 20);
		b1.addActionListener(this);
		b2.addActionListener(this);
		add(b1);
		add(b2);
		t6 = new JTextField(10);
		setVisible(true);
	}
	public static void main(String a[])
	{
		Window fr = new Window();
	}
	public void actionPerformed(ActionEvent e)
	{
		String s = null;
		String s1 = null, s2 = null, s3 = null;
		int l, i = 0;
		s2 = t6.getText();
		if (e.getSource() == b1)
		{
			i++;
			s = t1.getText();
			l = s.length();
			s1 = s.substring(0, 1);
			if (i < 2)
			{
				if(s2.equals(t5.getText()))
					t5.setText(s1);
				else if(s2.equals(t4.getText()))
					t4.setText(s1);
				else if(s2.equals(t3.getText()))
					t3.setText(s1);
				s3 = s.substring(1, l);
				t1.setText(s3);
			}
		}
		if (e.getSource() == b2)
		{
			String a,b,c;
			i--;
			b = t2.getText();
			a = t5.getText();
			t5.setText(t4.getText());
			t4.setText(t3.getText());
			t3.setText(s2);
			t2.setText(b + a);
		}
	}
}