import java.awt.*;  
import java.awt.event.*;
import java.applet.*;  	
/* <applet code="image.java" width = 300 height=300>
</applet> */ 
  
public class image extends Applet implements ActionListener
{  
	Image img_sun, img_cars;
	Button a1,a2,a3,a4;
	String st;
	int w_width, w_height;
	
	public void init()
	{
		w_width = this.getSize().width;
		w_height = this.getSize().height;
		a1 = new Button("Rising Sun");
		a2 = new Button("Cars");
		a3 = new Button("Draw Sun");
		add(a1);
		a1.addActionListener(this);
		add(a3);
		a3.addActionListener(this);
		add(a2);
		a2.addActionListener(this);
		a4 = new Button("Draw Car");
		add(a4);
		a4.addActionListener(this);
	    img_cars = getImage(getDocumentBase(),"cars.jpg");
		img_sun = getImage(getDocumentBase(),"risingsun.gif");
	}
    
	public void paint(Graphics g)
	{
		w_width = this.getSize().width;
		w_height = this.getSize().height;
		setBackground(Color.WHITE);
		if (st.equals("Rising Sun"))  
			g.drawImage(img_sun, 10, 35, w_width-20, w_height-70, this);
		else if (st.equals("Cars"))
	    	g.drawImage(img_cars, 10, 35, w_width-20, w_height-70, this);
		else if (st.equals("Draw Sun"))
		{
			setBackground(Color.WHITE);
			g.setColor(Color.YELLOW);
			g.fillOval(10, w_height/2, w_width-20, w_height);
		}  
		else
		{
			g.drawRect(30, 40, 300, 60);
			g.drawOval(60,100,50,50);
			g.drawOval(240,100,50,50);
			g.setColor(Color.RED);
			g.fillRect(30,40,300,60);
			g.setColor(Color.BLACK);
			g.fillOval(240,100,50,50);
			g.fillOval(60,100,50,50);
		}
	}

	public void actionPerformed(ActionEvent a)
	{
		st = a.getActionCommand();
		repaint();	
	}
}  
