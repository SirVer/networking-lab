//echo server.java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class SquareServer {
public static void main(String args[]){


	Socket s=null;
	ServerSocket ss2=null;
	System.out.println("Server Listening...");
	try{
		ss2 = new ServerSocket(4445); // can also use static final PORT_NUM , when defined

	}
	catch(IOException e){
	e.printStackTrace();
	System.out.println("Server error");

	}

	while(true){
		try{
			s= ss2.accept();
			System.out.println("New client connected.");
			ServerThread st=new ServerThread(s);
			st.start();

		}

	catch(Exception e){
		e.printStackTrace();
		System.out.println("Connection Error");

	}
	}

}

}

class ServerThread extends Thread{  

	String line=null;
	int result;
	BufferedReader  is = null;
	PrintWriter os=null;
	Socket s=null;

	public ServerThread(Socket s){
		this.s=s;
	}

	public void run() {
	try{
		is= new BufferedReader(new InputStreamReader(s.getInputStream()));
		os=new PrintWriter(s.getOutputStream());

	}catch(IOException e){
		System.out.println("IO error in server thread");
	}

	try {
		line=is.readLine();
		while(line.compareTo("STOP")!=0){
			String words=line;
			 
			int a=Integer.parseInt(words);
					result=a*a;
			os.println(Integer.toString(result));
			os.flush();
			System.out.println("Response to client: "+Integer.toString(result));
			line=is.readLine();
		}   
	} catch (IOException e) {

		line=this.getName(); //reused String line for getting thread name
		System.out.println("IO Error/ Client "+line+" terminated abruptly");
	}
	catch(NullPointerException e){
		line=this.getName(); //reused String line for getting thread name
		System.out.println("Client "+line+" Closed");
	}

	finally{    
	try{
		System.out.println("Connection closing...");
		if (is!=null){
			is.close(); 
			System.out.println(" Socket Input Stream Closed");
		}

		if(os!=null){
			os.close();
			System.out.println("Socket Out Closed");
		}
		if (s!=null){
		s.close();
		System.out.println("Socket Closed");
		}

		}
	catch(IOException ie){
		System.out.println("Socket Close Error");
	}
	}//end finally
	}
}